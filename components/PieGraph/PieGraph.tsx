import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import "./PieGraph.css";
import { Cell, Pie, PieChart, ResponsiveContainer, Legend } from "recharts";
import { render } from "react-dom";

interface IProps {
	data: Array<{
		name: string;
		value: number;
		percentage?: Array<string>;
	}>;
}

const legendColor = (value: string, entry: any) => {
	const { color } = entry;
	return <span style={{ color }}>{value}</span>;
}

const renderLegend = (props: any) => {
	const { payload, margin } = props;
	return (
	  <ul className='custom-label'> 
		{
		  payload.map((e: any, idx: number) => (
			<li style={{color:e.id}} key={`item-${idx}`} > <b>{e.value[0]}:</b> {e.value[1]}</li>
		  ))
		}
	  </ul>
	);
}

const COLORS = ["#FF4895", "#6E4FE9", "#1766FF", "#2D42FF",
	"#FF7CB3", "#967EF2", "#5A9CFF", '#3E53FF',
	"#EE6BA2", "#967EF2", "#5A8BEE", '#2D42EE'];

const PieGraph = ({ data }: IProps) => {
	const [data2, setData2] = useState<IProps["data"]>();

	useEffect(() => {
		let total = 0;
		for (let i = 0; i < data.length; i++) {
			total += data[i].value;
		}
		for (let i = 0; i < data.length; i++) {
			let temp = data;
			temp[i] = {
				...temp[i],
				percentage: [temp[i].name, `${((temp[i].value / total) * 100).toFixed(1)}%`],
			};
		}
		data.sort((a, b) => a.value - b.value);
		setData2([...data]);
	}, [data]);

	return (
		<ResponsiveContainer width="100%" height="100%">
			<PieChart margin={{ top: 0, left: 0, right: 0, bottom: 0 }}>
				<Pie
					data={data2}
					dataKey="value"
					nameKey="percentage"
					cx="50%"
					cy="50%"
					startAngle={0}
					endAngle={360}
					outerRadius="100%"
					fill="#82ca9d"
					label={false}
					labelLine={false}
					stroke={"none"}
				>
					{data.map((entry, index) => (
						<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
					))}
				</Pie>
				<Legend
					payload={ data2 ? data2.map( (d, i) => {return {value: d.percentage, type: 'circle', id: COLORS[i % COLORS.length] }; }) : [] }
					iconType='circle' align='right'
					layout='vertical' verticalAlign="top"
					margin={{top: 30, right: 10, left: 0, bottom: 0}}
					content={renderLegend}
					formatter={legendColor}
				/>
			</PieChart>
		</ResponsiveContainer>
	);
};

PieGraph.propTypes = {
	data: PropTypes.arrayOf(
		PropTypes.shape({
			name: PropTypes.string,
			value: PropTypes.number,
		}).isRequired
	).isRequired,
};

export default PieGraph;
